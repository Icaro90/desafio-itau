package com.example.desafioitau.service.impl;

import com.example.desafioitau.model.entity.CategoriaEntity;
import com.example.desafioitau.repository.CategoriaRepository;
import com.example.desafioitau.service.CategoriaService;
import com.example.desafioitau.model.response.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoriaServiceImpl implements CategoriaService {

    private static final Logger log = LoggerFactory.getLogger(CategoriaServiceImpl.class);

    @Autowired
    private CategoriaRepository categoriaRepository;

    @Override
    public List<CategoriaEntity> findAll() {
        try {
            return (List<CategoriaEntity>) categoriaRepository.findAll();
        } catch (ResponseException ex) {
            log.error("Erro ao obter lista de categorias = {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public Optional<CategoriaEntity> findById(Long id) {
        try {
            return categoriaRepository.findById(id);
        } catch(ResponseException ex) {
            log.error("Erro ao obter categoria pelo id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void create(CategoriaEntity categoria) {
        try {
            if (categoria != null) {
                categoriaRepository.save(categoria);
            } else {
                log.warn("Categoria não pode ser nula");
                throw new ResponseException("Categoria não pode ser nulo", HttpStatus.BAD_REQUEST);
            }
        } catch(ResponseException ex) {
            log.error("Erro ao tentar criar categoria, {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void update(CategoriaEntity categoria) {
        try {
            if (categoria != null) {
                categoriaRepository.save(categoria);
            } else {
                log.warn("Categoria não pode ser nula");
                throw new ResponseException("Categoria não pode ser nula", HttpStatus.BAD_REQUEST);
            }
        } catch(ResponseException ex) {
            log.error("Erro ao tentar atualizar categoria, {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void delete(Long id) {
        try {
            Optional<CategoriaEntity> categoria = categoriaRepository.findById(id);
            categoria.ifPresent(u -> categoriaRepository.delete(u));
        } catch(ResponseException ex) {
            log.error("Erro ao tentar deletar categoria pelo id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }
}
