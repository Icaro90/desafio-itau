package com.example.desafioitau.service.impl;

import com.example.desafioitau.model.entity.SubCategoriaEntity;
import com.example.desafioitau.repository.SubCategoriaRepository;
import com.example.desafioitau.service.SubCategoriaService;
import com.example.desafioitau.model.response.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SubCategoriaServiceImpl implements SubCategoriaService {

    private static final Logger log = LoggerFactory.getLogger(SubCategoriaServiceImpl.class);

    @Autowired
    private SubCategoriaRepository subCategoriaRepository;

    @Override
    public List<SubCategoriaEntity> findAll() {
        try {
            return (List<SubCategoriaEntity>) subCategoriaRepository.findAll();
        } catch (ResponseException ex) {
            log.error("Erro ao obter lista de sub-cartegorias = {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public Optional<SubCategoriaEntity> findById(Long id) {
        try {
            return subCategoriaRepository.findById(id);
        } catch(ResponseException ex) {
            log.error("Erro ao obter sub-categoria pelo id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void create(SubCategoriaEntity subCategoria) {
        try {
            if (subCategoria != null) {
                subCategoriaRepository.save(subCategoria);
            } else {
                log.warn("Sub-categoria não pode ser nulo");
                throw new ResponseException("Sub-categoria não pode ser nulo", HttpStatus.BAD_REQUEST);
            }
        } catch(ResponseException ex) {
            log.error("Erro ao tentar criar sub-categoria, {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void update(SubCategoriaEntity subCategoria) {
        try {
            if (subCategoria != null) {
                subCategoriaRepository.save(subCategoria);
            } else {
                log.warn("Sub-categoria não pode ser nula");
                throw new ResponseException("Sub-categoria não pode ser nula", HttpStatus.BAD_REQUEST);
            }
        } catch(ResponseException ex) {
            log.error("Erro ao tentar atualizar sub-categoria, {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void delete(Long id) {
        try {
            Optional<SubCategoriaEntity> subCategoria = subCategoriaRepository.findById(id);
            subCategoria.ifPresent(u -> subCategoriaRepository.delete(u));
        } catch(ResponseException ex) {
            log.error("Erro ao tentar deletar sub-categoria pelo id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }
}
