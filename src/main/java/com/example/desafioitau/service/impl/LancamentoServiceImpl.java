package com.example.desafioitau.service.impl;

import com.example.desafioitau.model.entity.LancamentoEntity;
import com.example.desafioitau.repository.LancamentoRepository;
import com.example.desafioitau.service.LancamentoService;
import com.example.desafioitau.model.response.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class LancamentoServiceImpl implements LancamentoService {

    private static final Logger log = LoggerFactory.getLogger(LancamentoServiceImpl.class);

    @Autowired
    private LancamentoRepository lancamentoRepository;

    @Override
    public List<LancamentoEntity> findAll() {
        try {
            return (List<LancamentoEntity>) lancamentoRepository.findAll();
        } catch (ResponseException ex) {
            log.error("Erro ao obter lista de lançamentos = {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public Optional<LancamentoEntity> findById(Long id) {
        try {
            return lancamentoRepository.findById(id);
        } catch(ResponseException ex) {
            log.error("Erro ao obter categoria pelo id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void create(LancamentoEntity lancamento) {
        try {
            if (lancamento != null) {
                lancamento.setData(lancamento.getData() == null ? LocalDate.now() : lancamento.getData());
                lancamentoRepository.save(lancamento);
            } else {
                log.warn("Lançamento não pode ser nulo");
                throw new ResponseException("Lançamento não pode ser nulo", HttpStatus.BAD_REQUEST);
            }
        } catch(ResponseException ex) {
            log.error("Erro ao tentar criar lançamento, {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void update(LancamentoEntity lancamento) {
        try {
            if (lancamento != null) {
                lancamentoRepository.save(lancamento);
            } else {
                log.warn("Lançamento não pode ser nulo");
                throw new ResponseException("Lançamento não pode ser nulo", HttpStatus.BAD_REQUEST);
            }
        } catch(ResponseException ex) {
            log.error("Erro ao tentar atualizar lançamento, {}", ex.getStatus());
            throw ex;
        }
    }

    @Override
    public void delete(Long id) {
        try {
            Optional<LancamentoEntity> lancamento = lancamentoRepository.findById(id);
            lancamento.ifPresent(u -> lancamentoRepository.delete(u));
        } catch(ResponseException ex) {
            log.error("Erro ao tentar deletar categoria pelo id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }

    @Override
    public List<LancamentoEntity> getAllByDataBetween(LocalDate dataInicio, LocalDate dataFim) {
        return lancamentoRepository.getAllByDataBetween(dataInicio, dataFim);
    }

    @Override
    public List<LancamentoEntity> getAllCustom(LocalDate dataInicio, LocalDate dataFim, Long idCategoria) {
        return lancamentoRepository.getAllCustom(dataInicio, dataFim, idCategoria);
    }

}
