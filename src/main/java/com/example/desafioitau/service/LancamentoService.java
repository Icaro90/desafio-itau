package com.example.desafioitau.service;

import com.example.desafioitau.model.entity.LancamentoEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface LancamentoService {

    List<LancamentoEntity> findAll();
    Optional<LancamentoEntity> findById(Long id);
    void create(LancamentoEntity user);
    void update(LancamentoEntity user);
    void delete(Long id);

    public List<LancamentoEntity> getAllByDataBetween(@Param("dataInicio") LocalDate dataInicio, @Param("dataFim")LocalDate dataFim);

    public List<LancamentoEntity> getAllCustom(@Param("dataInicio") LocalDate dataInicio, @Param("dataFim")LocalDate dataFim, @Param("idCategoria") Long idCategoria);

}
