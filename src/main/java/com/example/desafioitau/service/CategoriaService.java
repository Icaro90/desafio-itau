package com.example.desafioitau.service;

import com.example.desafioitau.model.entity.CategoriaEntity;

import java.util.List;
import java.util.Optional;

public interface CategoriaService {

    List<CategoriaEntity> findAll();
    Optional<CategoriaEntity> findById(Long id);
    void create(CategoriaEntity user);
    void update(CategoriaEntity user);
    void delete(Long id);
}
