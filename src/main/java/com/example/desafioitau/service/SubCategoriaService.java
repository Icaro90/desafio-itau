package com.example.desafioitau.service;

import com.example.desafioitau.model.entity.SubCategoriaEntity;

import java.util.List;
import java.util.Optional;

public interface SubCategoriaService {

    List<SubCategoriaEntity> findAll();
    Optional<SubCategoriaEntity> findById(Long id);
    void create(SubCategoriaEntity user);
    void update(SubCategoriaEntity user);
    void delete(Long id);
}
