package com.example.desafioitau;

public class AppConstants {

    //Messages system
    public static final String ERRO_VALIDACAO = "erro_validacao";
    public static final String NOME_OBRIGATORIO = "O campo 'nome' é obrigatorio";
    public static final String VALOR_OBRIGATORIO = "O campo 'valor' é obrigatorio";
    public static final String CATEGORIA_OBRIGATORIO = "O campo 'categoria' é obrigatorio";
    public static final String SUB_CATEGORIA_OBRIGATORIO = "O campo 'sub-categoria' é obrigatorio";
    public static final String DATA_INICIO = "O campo 'data inicio' é obrigatorio";
    public static final String DATA_FIM = "O campo 'data fim' é obrigatorio";
    public static final String DATA_INICIO_INVALIDA = "O campo 'data inicio' está invalido";
    public static final String DATA_FIM_INVALIDA = "O campo 'data fim' está invalido";

    //App variable
    public static final String FORMAT_DATA_RESPONSE_PATTERN = "dd/MM/yyyy";

}
