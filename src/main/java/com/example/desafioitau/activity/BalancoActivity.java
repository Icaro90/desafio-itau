package com.example.desafioitau.activity;

import com.example.desafioitau.AppConstants;
import com.example.desafioitau.model.entity.LancamentoEntity;
import com.example.desafioitau.model.response.BalancoResponseModel;
import com.example.desafioitau.service.LancamentoService;
import com.example.desafioitau.validation.BalancoValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class BalancoActivity {

    @Autowired
    LancamentoService lancamentoService;

    List<LancamentoEntity> lancamentos;

    BalancoConvertActivity balancoConvertActivity;

    public BalancoResponseModel getBalanco(String dataInicio, String dataFim, Long idCategoria){
        BalancoValidation balancoValidation = new BalancoValidation();

        if(balancoValidation.isValidRequest(dataInicio, dataFim)){
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(AppConstants.FORMAT_DATA_RESPONSE_PATTERN);

            LocalDate dataInicioFormat = LocalDate.parse(dataInicio, dateTimeFormatter);
            LocalDate dataFimFormat = LocalDate.parse(dataFim, dateTimeFormatter);

            if(idCategoria != null){
                lancamentos = lancamentoService.getAllCustom(dataInicioFormat, dataFimFormat, idCategoria);
            }else{
                lancamentos = lancamentoService.getAllByDataBetween(dataInicioFormat, dataFimFormat);
            }

            balancoConvertActivity = new BalancoConvertActivity();
            return balancoConvertActivity.getBalancoConverted(lancamentos, idCategoria);
        }
        return null;
    }
}
