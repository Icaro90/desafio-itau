package com.example.desafioitau.activity;

import com.example.desafioitau.model.entity.LancamentoEntity;
import com.example.desafioitau.model.response.BalancoResponseModel;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public class BalancoConvertActivity {

    public BalancoResponseModel getBalancoConverted(List<LancamentoEntity> lancamentos, Long idCategoria){
        BigDecimal receita = new BigDecimal(BigInteger.ZERO);
        BigDecimal despesa = new BigDecimal(BigInteger.ZERO);
        BigDecimal saldo = new BigDecimal(BigInteger.ZERO);

        for (LancamentoEntity lancamento: lancamentos) {
            if(lancamento.getValor().compareTo(new BigDecimal(BigInteger.ZERO)) < 0){
                despesa = despesa.add(lancamento.getValor());
            }else{
                receita = receita.add(lancamento.getValor());
            }
        }

        saldo = receita.add(despesa);

        BalancoResponseModel balanco = new BalancoResponseModel();
        if(idCategoria == null){
            balanco.setCategoria(lancamentos.get(0).getSubcategoria().getCategoria());
        }
        balanco.setReceita(receita);
        balanco.setDespesa(despesa);
        balanco.setSaldo(saldo);
        return balanco;
    }
}
