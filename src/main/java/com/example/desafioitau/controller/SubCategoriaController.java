package com.example.desafioitau.controller;

import com.example.desafioitau.model.response.ErrorResponseModel;
import com.example.desafioitau.model.entity.SubCategoriaEntity;
import com.example.desafioitau.service.SubCategoriaService;
import com.example.desafioitau.model.response.ResponseException;
import com.example.desafioitau.validation.SubCategoriaValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/subcategorias")
public class SubCategoriaController {
    private static final Logger log = LoggerFactory.getLogger(SubCategoriaController.class);

    @Autowired
    private SubCategoriaService subCategoriaService;

    private SubCategoriaValidation subCategoriaValidation;

    @RequestMapping(value = {""}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<SubCategoriaEntity> getAll() {
        try {
            return subCategoriaService.findAll();
        } catch (ResponseException ex) {
            log.error("Erro ao obter lista de sub-categorias = {}", ex.getStatus());
            throw ex;
        }
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Optional<SubCategoriaEntity> getById(@PathVariable("id") Long id) {
        try {
            return subCategoriaService.findById(id);
        } catch (ResponseException ex) {
            log.error("Erro ao obter sub-categoria pelo id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }

    @RequestMapping(value = {""}, method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<?> create(@RequestBody SubCategoriaEntity subCategoria) {
        try {
            subCategoriaValidation = new SubCategoriaValidation();
            if(subCategoriaValidation.isValidRequest(subCategoria)){
                subCategoriaService.create(subCategoria);

                log.info("Nova sub-categoria {} criada com sucesso", subCategoria.getNome());
            }

            return new ResponseEntity<>("Sub-categoria criada com sucesso!", HttpStatus.CREATED);
        } catch (ResponseException ex) {
            return new ResponseEntity<>(new ErrorResponseModel(ex.getCodeError(), ex.getMessage()), ex.getStatus());
        }
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.PUT, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<?> update(@RequestBody SubCategoriaEntity subCategoria, @PathVariable("id") Long id) {
        try {
            subCategoriaValidation = new SubCategoriaValidation();
            if(subCategoriaValidation.isValidRequest(subCategoria)){
                subCategoria.setIdSubCategoria(id);
                subCategoriaService.update(subCategoria);

                log.info("Sub-categoria {} atualizada.", subCategoria.getIdSubCategoria());
            }

            return new ResponseEntity<>("Sub-categoria atualizada com sucesso!", HttpStatus.OK);
        } catch (ResponseException ex) {
            return new ResponseEntity<>(new ErrorResponseModel(ex.getCodeError(), ex.getMessage()), ex.getStatus());
        }
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@PathVariable("id") Long id) {
        try {
            subCategoriaService.delete(id);
            return String.format("Sub-categoria %s deletada com sucesso!", id);
        } catch (ResponseException ex) {
            return String.format("Erro ao tentar excluir sub-categoria com id = %s, %s", id, ex.getStatus());
        }
    }
}
