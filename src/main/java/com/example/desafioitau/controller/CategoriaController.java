package com.example.desafioitau.controller;

import com.example.desafioitau.model.response.ErrorResponseModel;
import com.example.desafioitau.model.entity.CategoriaEntity;
import com.example.desafioitau.service.CategoriaService;
import com.example.desafioitau.model.response.ResponseException;
import com.example.desafioitau.validation.CategoriaValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/categorias")
public class CategoriaController {
    private static final Logger log = LoggerFactory.getLogger(CategoriaController.class);

    @Autowired
    private CategoriaService categoriaService;

    private CategoriaValidation categoriaValidation;

    @RequestMapping(value = {""}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<CategoriaEntity> getAll() {
        try {
            return categoriaService.findAll();
        } catch (ResponseException ex) {
            log.error("Erro ao obter lista de categorias = {}", ex.getStatus());
            throw ex;
        }
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Optional<CategoriaEntity> getById(@PathVariable("id") Long id) {
        try {
            return categoriaService.findById(id);
        } catch (ResponseException ex) {
            log.error("Erro ao obter categoria pelo id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }

    @RequestMapping(value = {""}, method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<?> create(@RequestBody CategoriaEntity categoria) {
        try {
            categoriaValidation = new CategoriaValidation();
            if(categoriaValidation.isValidRequest(categoria)){
                categoriaService.create(categoria);

                log.info("Nova categoria {} criado com sucesso", categoria.getNome());
            }

            return new ResponseEntity<>("Categoria criada com sucesso!", HttpStatus.CREATED);
        } catch (ResponseException ex) {
            return new ResponseEntity<>(new ErrorResponseModel(ex.getCodeError(), ex.getMessage()), ex.getStatus());
        }
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.PUT, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<?> update(@RequestBody CategoriaEntity categoria, @PathVariable("id") Long id) {
        try {
            categoriaValidation = new CategoriaValidation();
            if(categoriaValidation.isValidRequest(categoria)){
                categoria.setIdCategoria(id);
                categoriaService.update(categoria);

                log.info("Categoria {} atualizada.", categoria.getIdCategoria());
            }

            return new ResponseEntity<>("Categoria atualizada com sucesso!", HttpStatus.OK);
        } catch (ResponseException ex) {
            return new ResponseEntity<>(new ErrorResponseModel(ex.getCodeError(), ex.getMessage()), ex.getStatus());
        }
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@PathVariable("id") Long id) {
        try {
            categoriaService.delete(id);
            return String.format("Categoria %s deletada com sucesso!", id);
        } catch (ResponseException ex) {
            return String.format("Erro ao tentar excluir categoria com id = %s, %s", id, ex.getStatus());
        }
    }
}
