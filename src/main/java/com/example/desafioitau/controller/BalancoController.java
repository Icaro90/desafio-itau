package com.example.desafioitau.controller;

import com.example.desafioitau.AppConstants;
import com.example.desafioitau.activity.BalancoActivity;
import com.example.desafioitau.model.response.BalancoResponseModel;
import com.example.desafioitau.model.response.ErrorResponseModel;
import com.example.desafioitau.service.LancamentoService;
import com.example.desafioitau.model.response.ResponseException;
import com.example.desafioitau.utils.DateValidatorFormatter;
import com.example.desafioitau.validation.BalancoValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@RestController
public class BalancoController {
    private static final Logger log = LoggerFactory.getLogger(BalancoController.class);

    @Autowired
    private LancamentoService lancamentoService;

    @Autowired
    private BalancoActivity balancoActivity;

    @GetMapping("/balanco")
    @ResponseBody
    public ResponseEntity<?> getById(@RequestParam("data_inicio") String dataInicio,
                                        @RequestParam("data_fim") String dataFim,
                                        @RequestParam(value = "id_categoria", required = false) Long idCategoria) {
        try {
            return new ResponseEntity<>(balancoActivity.getBalanco(dataInicio, dataFim, idCategoria), HttpStatus.OK);
        } catch (ResponseException ex) {
            return new ResponseEntity<>(new ErrorResponseModel(ex.getCodeError(), ex.getMessage()), ex.getStatus());
        }
    }
}