package com.example.desafioitau.controller;

import com.example.desafioitau.model.response.ErrorResponseModel;
import com.example.desafioitau.model.entity.LancamentoEntity;
import com.example.desafioitau.service.LancamentoService;
import com.example.desafioitau.model.response.ResponseException;
import com.example.desafioitau.validation.LancamentoValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/lancamentos")
public class LancamentoController {
    private static final Logger log = LoggerFactory.getLogger(LancamentoController.class);

    @Autowired
    private LancamentoService lancamentoService;

    private LancamentoValidation lancamentoValidation;

    @RequestMapping(value = {""}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<LancamentoEntity> getAll() {
        try {
            return lancamentoService.findAll();
        } catch (ResponseException ex) {
            log.error("Erro ao obter lista de lançamentos = {}", ex.getStatus());
            throw ex;
        }
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Optional<LancamentoEntity> getById(@PathVariable("id") Long id) {
        try {
            return lancamentoService.findById(id);
        } catch (ResponseException ex) {
            log.error("Erro ao obter lançamento pelo id = {}, {}", id, ex.getStatus());
            throw ex;
        }
    }

    @RequestMapping(value = {""}, method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<?> create(@RequestBody LancamentoEntity lancamento) {
        try {
            lancamentoValidation = new LancamentoValidation();
            if(lancamentoValidation.isValidRequest(lancamento)){
                lancamentoService.create(lancamento);

                log.info("Novo lançamento {} criado com sucesso", lancamento.getComentario());
            }

            return new ResponseEntity<>("Lançamento criado com sucesso!", HttpStatus.CREATED);
        } catch (ResponseException ex) {
            return new ResponseEntity<>(new ErrorResponseModel(ex.getCodeError(), ex.getMessage()), ex.getStatus());
        }
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.PUT, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<?> update(@RequestBody LancamentoEntity lancamento, @PathVariable("id") Long id) {
        try {
            lancamentoValidation = new LancamentoValidation();
            if(lancamentoValidation.isValidRequest(lancamento)){
                lancamento.setIdLancamento(id);
                lancamentoService.update(lancamento);

                log.info("Lançamento {} atualizado.", lancamento.getIdLancamento());
            }

            return new ResponseEntity<>("Lançamento atualizado com sucesso!", HttpStatus.OK);
        } catch (ResponseException ex) {
            return new ResponseEntity<>(new ErrorResponseModel(ex.getCodeError(), ex.getMessage()), ex.getStatus());
        }
    }

    @RequestMapping(value = {"/{id}"}, method = RequestMethod.DELETE)
    @ResponseBody
    public String delete(@PathVariable("id") Long id) {
        try {
            lancamentoService.delete(id);
            return String.format("Lançamento %s deletado com sucesso!", id);
        } catch (ResponseException ex) {
            return String.format("Erro ao tentar excluir lançamento com id = %s, %s", id, ex.getStatus());
        }
    }
}