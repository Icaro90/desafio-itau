package com.example.desafioitau.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "lancamento")
public class LancamentoEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_lancamento")
    private Long idLancamento;

    private BigDecimal valor;

    private LocalDate data;

    @ManyToOne
    @JoinColumn(name = "id_subcategoria", nullable = false)
    private SubCategoriaEntity subcategoria;

    private String comentario;

    public LancamentoEntity(){}
}
