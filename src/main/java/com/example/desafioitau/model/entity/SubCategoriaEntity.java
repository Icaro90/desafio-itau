package com.example.desafioitau.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "subcategoria")
public class SubCategoriaEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_subcategoria")
    private Long idSubCategoria;

    private String nome;

    @ManyToOne
    @JoinColumn(name = "id_categoria", nullable = false)
    private CategoriaEntity categoria;

    public SubCategoriaEntity(){};
    public SubCategoriaEntity(Long idSubCategoria){
        this.idSubCategoria = idSubCategoria;
    }

}
