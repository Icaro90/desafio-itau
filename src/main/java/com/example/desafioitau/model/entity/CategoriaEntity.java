package com.example.desafioitau.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "categoria")
public class CategoriaEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_categoria")
    private Long idCategoria;

    private String nome;

    public CategoriaEntity(){}
    public CategoriaEntity(Long idCategoria){
        this.idCategoria = idCategoria;
    }

}
