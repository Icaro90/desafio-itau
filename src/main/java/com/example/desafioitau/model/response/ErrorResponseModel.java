package com.example.desafioitau.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ErrorResponseModel {

    private String codigo;
    private String mensagem;
}
