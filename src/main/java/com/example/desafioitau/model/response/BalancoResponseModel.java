package com.example.desafioitau.model.response;

import com.example.desafioitau.model.entity.CategoriaEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BalancoResponseModel {

    private CategoriaEntity categoria;
    private BigDecimal receita;
    private BigDecimal despesa;
    private BigDecimal saldo;

}
