package com.example.desafioitau.model.response;

import org.springframework.http.HttpStatus;

public class ResponseException extends RuntimeException{
    private HttpStatus status;
    private String codeError;

    public ResponseException(String codeError, String message) {
        super(message);
        this.codeError = codeError;
    }

    public ResponseException(String codeError, String message, HttpStatus status) {
        super(message);
        this.codeError = codeError;
        this.status = status;
    }

    public ResponseException(String message) {
        super(message);
    }

    public ResponseException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public ResponseException(String message, Throwable cause, HttpStatus status) {
        super(message, cause);
        this.status = status;
    }

    public HttpStatus getStatus() { return status; }
    public String getCodeError() { return codeError; }
}
