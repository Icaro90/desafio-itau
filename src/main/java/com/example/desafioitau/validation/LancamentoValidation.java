package com.example.desafioitau.validation;

import com.example.desafioitau.AppConstants;
import com.example.desafioitau.model.entity.LancamentoEntity;
import com.example.desafioitau.model.response.ResponseException;
import org.springframework.http.HttpStatus;

public class LancamentoValidation {

    public Boolean isValidRequest(LancamentoEntity lancamento) {

        if(lancamento.getValor() == null){
            throw new ResponseException(AppConstants.ERRO_VALIDACAO, AppConstants.VALOR_OBRIGATORIO, HttpStatus.BAD_REQUEST);
        }

        if(lancamento != null && lancamento.getSubcategoria() == null){
            throw new ResponseException(AppConstants.ERRO_VALIDACAO, AppConstants.SUB_CATEGORIA_OBRIGATORIO, HttpStatus.BAD_REQUEST);
        }

        return true;
    }

}
