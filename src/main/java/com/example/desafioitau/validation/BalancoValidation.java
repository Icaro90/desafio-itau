package com.example.desafioitau.validation;

import com.example.desafioitau.AppConstants;
import com.example.desafioitau.model.response.ResponseException;
import com.example.desafioitau.utils.DateValidatorFormatter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

import java.time.format.DateTimeFormatter;

public class BalancoValidation {

    public Boolean isValidRequest(String dataInicio, String dataFim) {
        if (StringUtils.isBlank(dataInicio)) {
            throw new ResponseException(AppConstants.ERRO_VALIDACAO, AppConstants.DATA_INICIO, HttpStatus.BAD_REQUEST);
        }
        if (StringUtils.isBlank(dataFim)) {
            throw new ResponseException(AppConstants.ERRO_VALIDACAO, AppConstants.DATA_FIM, HttpStatus.BAD_REQUEST);
        }

        DateValidatorFormatter dateValidator = new DateValidatorFormatter(DateTimeFormatter.ofPattern(AppConstants.FORMAT_DATA_RESPONSE_PATTERN));
        if (!dateValidator.isValid(dataInicio)) {
            throw new ResponseException(AppConstants.ERRO_VALIDACAO, AppConstants.DATA_INICIO_INVALIDA, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (!dateValidator.isValid(dataFim)) {
            throw new ResponseException(AppConstants.ERRO_VALIDACAO, AppConstants.DATA_FIM_INVALIDA, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return true;
    }
}
