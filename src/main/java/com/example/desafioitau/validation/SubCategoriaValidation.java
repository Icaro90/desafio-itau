package com.example.desafioitau.validation;

import com.example.desafioitau.AppConstants;
import com.example.desafioitau.model.entity.SubCategoriaEntity;
import com.example.desafioitau.model.response.ResponseException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

public class SubCategoriaValidation {

    public Boolean isValidRequest(SubCategoriaEntity subCategoria) {

        if(StringUtils.isBlank(subCategoria.getNome())){
            throw new ResponseException(AppConstants.ERRO_VALIDACAO, AppConstants.NOME_OBRIGATORIO, HttpStatus.BAD_REQUEST);
        }

        if(subCategoria != null && subCategoria.getCategoria() == null){
            throw new ResponseException(AppConstants.ERRO_VALIDACAO, AppConstants.CATEGORIA_OBRIGATORIO, HttpStatus.BAD_REQUEST);
        }

        return true;
    }
}
