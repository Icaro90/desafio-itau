package com.example.desafioitau.validation;

import com.example.desafioitau.AppConstants;
import com.example.desafioitau.model.entity.CategoriaEntity;
import com.example.desafioitau.model.response.ResponseException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;

public class CategoriaValidation {

    public Boolean isValidRequest(CategoriaEntity categoria) {

        if(StringUtils.isBlank(categoria.getNome())){
            throw new ResponseException(AppConstants.ERRO_VALIDACAO, AppConstants.NOME_OBRIGATORIO, HttpStatus.BAD_REQUEST);
        }

        return true;
    }
}
