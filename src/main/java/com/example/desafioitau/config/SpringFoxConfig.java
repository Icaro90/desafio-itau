package com.example.desafioitau.config;

import org.assertj.core.util.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
@Import(SpringDataRestConfiguration.class)
public class SpringFoxConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.desafioitau"))
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(Lists.newArrayList(new ApiKey("apikey", HttpHeaders.AUTHORIZATION, "header")))
                .apiInfo(apiInfo());
    }


    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Inter Desafio API",
                "Código desafio para o inter",
                "1.0.0",
                "",
                new Contact("Icaro Noventa", "https://www.linkedin.com/in/icaro-noventa-4480616a/", "icaro.nove-nta@hotmail.com"),
                "", "", Collections.emptyList());
    }

}
