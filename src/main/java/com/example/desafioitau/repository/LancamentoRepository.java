package com.example.desafioitau.repository;

import com.example.desafioitau.model.entity.LancamentoEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface LancamentoRepository extends CrudRepository<LancamentoEntity, Long> {
    public List<LancamentoEntity> getAllByDataBetween(@Param("dataInicio") LocalDate dataInicio, @Param("dataFim")LocalDate dataFim);
    @Query(value = "select * from lancamento as l " +
            "JOIN subcategoria as sub ON l.id_subcategoria = sub.id_subcategoria " +
            "JOIN categoria as c ON sub.id_categoria = c.id_categoria " +
            "WHERE l.data BETWEEN :dataInicio AND :dataFim " +
            "AND c.id_categoria = :idCategoria ", nativeQuery = true)
    public List<LancamentoEntity> getAllCustom(@Param("dataInicio") LocalDate dataInicio, @Param("dataFim")LocalDate dataFim, @Param("idCategoria") Long idCategoria);
}
