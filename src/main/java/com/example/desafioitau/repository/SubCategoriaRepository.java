package com.example.desafioitau.repository;

import com.example.desafioitau.model.entity.SubCategoriaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubCategoriaRepository extends CrudRepository<SubCategoriaEntity, Long> {
}
