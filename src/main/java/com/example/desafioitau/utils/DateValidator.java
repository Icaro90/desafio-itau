package com.example.desafioitau.utils;

public interface DateValidator{
    boolean isValid(String dateStr);
}
