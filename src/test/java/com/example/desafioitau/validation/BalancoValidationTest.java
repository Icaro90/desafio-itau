package com.example.desafioitau.validation;

import com.example.desafioitau.AppConstants;
import com.example.desafioitau.model.response.ResponseException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

@RunWith(MockitoJUnitRunner.class)
public class BalancoValidationTest {

    BalancoValidation balancoValidation;

    @Test
    public void isValidRequestTrue() {
        balancoValidation = new BalancoValidation();
        Assert.assertTrue(balancoValidation.isValidRequest("10/10/2010", "11/10/2010"));
    }

    @Test(expected = ResponseException.class)
    public void isValidRequestError() {
        balancoValidation = new BalancoValidation();

        Boolean response = balancoValidation.isValidRequest("10/10/2010010", "10/10/2010010");
        Assert.assertEquals(new ResponseException(AppConstants.ERRO_VALIDACAO,
            AppConstants.DATA_INICIO_INVALIDA,
            HttpStatus.INTERNAL_SERVER_ERROR), response);
    }

}
