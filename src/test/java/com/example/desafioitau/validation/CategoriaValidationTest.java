package com.example.desafioitau.validation;

import com.example.desafioitau.AppConstants;
import com.example.desafioitau.model.entity.CategoriaEntity;
import com.example.desafioitau.model.response.ResponseException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

@RunWith(MockitoJUnitRunner.class)
public class CategoriaValidationTest {

    CategoriaValidation categoriaValidation;

    @Test
    public void isValidRequestTrue() {
        categoriaValidation = new CategoriaValidation();
        Boolean response = categoriaValidation.isValidRequest(newCategoria("Transporte"));

        Assert.assertTrue(response);
    }

    @Test(expected = ResponseException.class)
    public void isValidRequestError() {
        categoriaValidation = new CategoriaValidation();
        Boolean response = categoriaValidation.isValidRequest(newCategoria(null));

        Assert.assertEquals(new ResponseException(AppConstants.ERRO_VALIDACAO,
                AppConstants.NOME_OBRIGATORIO,
                HttpStatus.BAD_REQUEST), response);
    }

    public CategoriaEntity newCategoria(String nome) {
        return new CategoriaEntity((long) 1, nome);
    }
}
