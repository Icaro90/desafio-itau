package com.example.desafioitau.validation;

import com.example.desafioitau.AppConstants;
import com.example.desafioitau.model.entity.LancamentoEntity;
import com.example.desafioitau.model.entity.SubCategoriaEntity;
import com.example.desafioitau.model.response.ResponseException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.time.LocalDate;

@RunWith(MockitoJUnitRunner.class)
public class LancamentoValidationTest {
    @Mock
    LancamentoValidation lancamentoValidation;

    @Test
    public void isValidRequestTrue() {
        LancamentoEntity lancamento = newLancamento(new BigDecimal(2000.10), new SubCategoriaEntity((long)1));
        lancamentoValidation = new LancamentoValidation();
        Assert.assertTrue(lancamentoValidation.isValidRequest(lancamento));
    }

    @Test(expected = ResponseException.class)
    public void isValidRequestError() {
        LancamentoEntity lancamento = newLancamento(null, null);
        lancamentoValidation = new LancamentoValidation();
        Boolean response = lancamentoValidation.isValidRequest(lancamento);

        Assert.assertEquals(new ResponseException(AppConstants.ERRO_VALIDACAO,
                AppConstants.VALOR_OBRIGATORIO,
                HttpStatus.BAD_REQUEST), response);
    }

    public LancamentoEntity newLancamento(BigDecimal valor, SubCategoriaEntity subCategoria) {
        return new LancamentoEntity((long) 1, valor, LocalDate.now(), subCategoria, "teste comentario");
    }
}
