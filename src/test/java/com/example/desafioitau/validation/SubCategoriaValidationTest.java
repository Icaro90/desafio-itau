package com.example.desafioitau.validation;

import com.example.desafioitau.AppConstants;
import com.example.desafioitau.model.entity.CategoriaEntity;
import com.example.desafioitau.model.entity.SubCategoriaEntity;
import com.example.desafioitau.model.response.ResponseException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

@RunWith(MockitoJUnitRunner.class)
public class SubCategoriaValidationTest {
    @Mock
    SubCategoriaValidation subCategoriaValidation;

    @Test
    public void isValidRequestTrue() {
        SubCategoriaEntity subCategoria = newSubCategoria("Transporte", new CategoriaEntity((long) 1));
        subCategoriaValidation = new SubCategoriaValidation();
        Assert.assertTrue(subCategoriaValidation.isValidRequest(subCategoria));
    }

    @Test(expected = ResponseException.class)
    public void isValidRequestError() {
        SubCategoriaEntity subCategoria = newSubCategoria(null, new CategoriaEntity((long) 1));
        subCategoriaValidation = new SubCategoriaValidation();
        Boolean response = subCategoriaValidation.isValidRequest(subCategoria);

        Assert.assertEquals(new ResponseException(AppConstants.ERRO_VALIDACAO,
            AppConstants.NOME_OBRIGATORIO,
            HttpStatus.BAD_REQUEST), response);
    }

    public SubCategoriaEntity newSubCategoria(String nome, CategoriaEntity categoria) {
        return new SubCategoriaEntity((long) 1, nome, categoria);
    }
}
