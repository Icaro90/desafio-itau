package com.example.desafioitau.service;

import com.example.desafioitau.model.entity.CategoriaEntity;
import com.example.desafioitau.model.entity.LancamentoEntity;
import com.example.desafioitau.model.entity.SubCategoriaEntity;
import com.example.desafioitau.repository.LancamentoRepository;
import com.example.desafioitau.service.impl.LancamentoServiceImpl;
import com.example.desafioitau.model.response.ResponseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class LancamentoServiceImplTest {
    @Mock
    LancamentoRepository lancamentoRepository;

    @InjectMocks
    @Spy
    LancamentoServiceImpl lancamentoService;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void findAll() {
        List<LancamentoEntity> response = new ArrayList<>();
        Mockito.when(lancamentoRepository.findAll()).thenReturn(response);

        lancamentoService.findAll();
    }

    @Test(expected = ResponseException.class)
    public void findAllException() {
        Mockito.when(lancamentoRepository.findAll()).thenThrow(new ResponseException("Exception for findAll"));

        lancamentoService.findAll();
    }

    @Test
    public void findById() {
        Optional<LancamentoEntity> response = Optional.of(newLancamento());
        Mockito.when(lancamentoRepository.findById((long) 1)).thenReturn(response);

        lancamentoService.findById((long) 1);
    }

    @Test(expected = ResponseException.class)
    public void findByIdNull() {
        Mockito.when(lancamentoRepository.findById(null)).thenThrow(new ResponseException("Exception for findById"));

        lancamentoService.findById(null);
    }

    @Test
    public void create() {
        LancamentoEntity lancamento = newLancamento();

        Mockito.when(lancamentoRepository.save(lancamento)).thenReturn(lancamento);

        lancamentoService.create(lancamento);

        Mockito.verify(lancamentoService, Mockito.atMostOnce()).create(lancamento);
    }

    @Test(expected = ResponseException.class)
    public void createNull() {

        lancamentoService.create(null);

        Mockito.verify(lancamentoService, Mockito.atMostOnce()).create(null);
    }

    @Test
    public void update() {
        LancamentoEntity lancamento = newLancamento();

        Mockito.when(lancamentoRepository.save(lancamento)).thenReturn(lancamento);

        lancamentoService.update(lancamento);

        Mockito.verify(lancamentoService, Mockito.atMostOnce()).update(lancamento);
    }

    @Test(expected = ResponseException.class)
    public void updateNull() {
        lancamentoService.update(null);

        Mockito.verify(lancamentoService, Mockito.atMostOnce()).update(null);
    }

    @Test
    public void delete() {
        Optional<LancamentoEntity> response = Optional.of(newLancamento());
        Mockito.when(lancamentoRepository.findById((long) 1)).thenReturn(response);
        Mockito.doNothing().when(lancamentoRepository).delete(response.get());

        lancamentoService.delete((long) 1);

        Mockito.verify(lancamentoService, Mockito.atMostOnce()).delete((long) 1);
    }

    @Test(expected = ResponseException.class)
    public void deleteThrowsException() {
        Optional<LancamentoEntity> response = Optional.of(newLancamento());

        Mockito.when(lancamentoRepository.findById((long) 0)).thenThrow(new ResponseException("Delete exception"));

        lancamentoService.delete((long) 0);

        Mockito.verify(lancamentoService, Mockito.atMostOnce()).delete((long) 0);
    }

    public LancamentoEntity newLancamento() {
        return new LancamentoEntity((long) 1, new BigDecimal(2000.10), LocalDate.now(),
                new SubCategoriaEntity((long) 1, "sub-teste", new CategoriaEntity((long) 1, "categoria-teste")), "teste comentario");
    }
}