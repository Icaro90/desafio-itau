package com.example.desafioitau.service;

import com.example.desafioitau.model.entity.CategoriaEntity;
import com.example.desafioitau.repository.CategoriaRepository;
import com.example.desafioitau.service.impl.CategoriaServiceImpl;
import com.example.desafioitau.model.response.ResponseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class CategoriaServiceImplTest {
    @Mock
    CategoriaRepository categoriaRepository;

    @InjectMocks
    @Spy
    CategoriaServiceImpl categoriaService;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void findAll() {
        List<CategoriaEntity> response = new ArrayList<>();
        Mockito.when(categoriaRepository.findAll()).thenReturn(response);

        categoriaService.findAll();
    }

    @Test(expected = ResponseException.class)
    public void findAllException() {
        Mockito.when(categoriaRepository.findAll()).thenThrow(new ResponseException("Exception for findAll"));

        categoriaService.findAll();
    }

    @Test
    public void findById() {
        Optional<CategoriaEntity> response = Optional.of(newCategoria());
        Mockito.when(categoriaRepository.findById((long) 1)).thenReturn(response);

        categoriaService.findById((long) 1);
    }

    @Test(expected = ResponseException.class)
    public void findByIdNull() {
        Mockito.when(categoriaRepository.findById(null)).thenThrow(new ResponseException("Exception for findById"));

        categoriaService.findById(null);
    }

    @Test
    public void create() {
        CategoriaEntity categoria = newCategoria();

        Mockito.when(categoriaRepository.save(categoria)).thenReturn(categoria);

        categoriaService.create(categoria);

        Mockito.verify(categoriaService, Mockito.atMostOnce()).create(categoria);
    }

    @Test(expected = ResponseException.class)
    public void createNull() {

        categoriaService.create(null);

        Mockito.verify(categoriaService, Mockito.atMostOnce()).create(null);
    }

    @Test
    public void update() {
        CategoriaEntity categoria = newCategoria();

        Mockito.when(categoriaRepository.save(categoria)).thenReturn(categoria);

        categoriaService.update(categoria);

        Mockito.verify(categoriaService, Mockito.atMostOnce()).update(categoria);
    }

    @Test(expected = ResponseException.class)
    public void updateNull() {
        categoriaService.update(null);

        Mockito.verify(categoriaService, Mockito.atMostOnce()).update(null);
    }

    @Test
    public void delete() {
        Optional<CategoriaEntity> response = Optional.of(newCategoria());
        Mockito.when(categoriaRepository.findById((long) 1)).thenReturn(response);
        Mockito.doNothing().when(categoriaRepository).delete(response.get());

        categoriaService.delete((long) 1);

        Mockito.verify(categoriaService, Mockito.atMostOnce()).delete((long) 1);
    }

    @Test(expected = ResponseException.class)
    public void deleteThrowsException() {
        Optional<CategoriaEntity> response = Optional.of(newCategoria());

        Mockito.when(categoriaRepository.findById((long) 0)).thenThrow(new ResponseException("Delete exception"));

        categoriaService.delete((long) 0);

        Mockito.verify(categoriaService, Mockito.atMostOnce()).delete((long) 0);
    }

    public CategoriaEntity newCategoria() {
        return new CategoriaEntity((long) 1, "Transporte");
    }
}