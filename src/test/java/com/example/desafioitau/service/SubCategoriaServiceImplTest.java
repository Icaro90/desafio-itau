package com.example.desafioitau.service;

import com.example.desafioitau.model.entity.CategoriaEntity;
import com.example.desafioitau.model.entity.SubCategoriaEntity;
import com.example.desafioitau.repository.SubCategoriaRepository;
import com.example.desafioitau.service.impl.SubCategoriaServiceImpl;
import com.example.desafioitau.model.response.ResponseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class SubCategoriaServiceImplTest {
    @Mock
    SubCategoriaRepository subCategoriaRepository;

    @InjectMocks
    @Spy
    SubCategoriaServiceImpl subCategoriaService;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void findAll() {
        List<SubCategoriaEntity> response = new ArrayList<>();
        Mockito.when(subCategoriaRepository.findAll()).thenReturn(response);

        subCategoriaService.findAll();
    }

    @Test(expected = ResponseException.class)
    public void findAllException() {
        Mockito.when(subCategoriaRepository.findAll()).thenThrow(new ResponseException("Exception for findAll"));

        subCategoriaService.findAll();
    }

    @Test
    public void findById() {
        Optional<SubCategoriaEntity> response = Optional.of(newSubCategoria());
        Mockito.when(subCategoriaRepository.findById((long) 1)).thenReturn(response);

        subCategoriaService.findById((long) 1);
    }

    @Test(expected = ResponseException.class)
    public void findByIdNull() {
        Mockito.when(subCategoriaRepository.findById(null)).thenThrow(new ResponseException("Exception for findById"));

        subCategoriaService.findById(null);
    }

    @Test
    public void create() {
        SubCategoriaEntity subCategoria = newSubCategoria();

        Mockito.when(subCategoriaRepository.save(subCategoria)).thenReturn(subCategoria);

        subCategoriaService.create(subCategoria);

        Mockito.verify(subCategoriaService, Mockito.atMostOnce()).create(subCategoria);
    }

    @Test(expected = ResponseException.class)
    public void createNull() {

        subCategoriaService.create(null);

        Mockito.verify(subCategoriaService, Mockito.atMostOnce()).create(null);
    }

    @Test
    public void update() {
        SubCategoriaEntity subCategoria = newSubCategoria();

        Mockito.when(subCategoriaRepository.save(subCategoria)).thenReturn(subCategoria);

        subCategoriaService.update(subCategoria);

        Mockito.verify(subCategoriaService, Mockito.atMostOnce()).update(subCategoria);
    }

    @Test(expected = ResponseException.class)
    public void updateNull() {
        subCategoriaService.update(null);

        Mockito.verify(subCategoriaService, Mockito.atMostOnce()).update(null);
    }

    @Test
    public void delete() {
        Optional<SubCategoriaEntity> response = Optional.of(newSubCategoria());
        Mockito.when(subCategoriaRepository.findById((long) 1)).thenReturn(response);
        Mockito.doNothing().when(subCategoriaRepository).delete(response.get());

        subCategoriaService.delete((long) 1);

        Mockito.verify(subCategoriaService, Mockito.atMostOnce()).delete((long) 1);
    }

    @Test(expected = ResponseException.class)
    public void deleteThrowsException() {
        Optional<SubCategoriaEntity> response = Optional.of(newSubCategoria());

        Mockito.when(subCategoriaRepository.findById((long) 0)).thenThrow(new ResponseException("Delete exception"));

        subCategoriaService.delete((long) 0);

        Mockito.verify(subCategoriaService, Mockito.atMostOnce()).delete((long) 0);
    }

    public SubCategoriaEntity newSubCategoria() {
        return new SubCategoriaEntity((long) 1, "Transporte", new CategoriaEntity((long) 1, "categoria teste"));
    }
}
