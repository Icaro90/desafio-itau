package com.example.desafioitau.utils;

import com.example.desafioitau.AppConstants;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.format.DateTimeFormatter;

@RunWith(MockitoJUnitRunner.class)
public class DateValidatorFormatterTest {

    @Mock
    DateValidatorFormatter dateValidatorFormatter;

    @Test
    public void isValidDate(){
        DateValidatorFormatter dateValidator = new DateValidatorFormatter(DateTimeFormatter.ofPattern(AppConstants.FORMAT_DATA_RESPONSE_PATTERN));

        Assert.assertTrue(dateValidator.isValid("09/11/1995"));
    }

    @Test
    public void isNotValidDate(){
        DateValidatorFormatter dateValidator = new DateValidatorFormatter(DateTimeFormatter.ofPattern(AppConstants.FORMAT_DATA_RESPONSE_PATTERN));

        Assert.assertFalse(dateValidator.isValid(" "));
    }

}
