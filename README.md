# Desafio Itaú

API em Java utilizando SpringBoot.

Execução


1.Na raiz do diretório, utilize o comando mvn compile compilar o projeto.
2.use o comando mvn spring-boot:run para executar o projeto
3.A documentação da API pode ser acessada através do endereço local http://localhost:8080/api/swagger-ui/index.html


Testes Unitários

1.Para executar testes unitários, utilize o comando mvn test
